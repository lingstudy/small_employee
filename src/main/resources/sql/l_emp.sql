/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 80011
 Source Host           : localhost:3306
 Source Schema         : l_emp

 Target Server Type    : MySQL
 Target Server Version : 80011
 File Encoding         : 65001

 Date: 15/09/2020 14:19:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_department
-- ----------------------------
DROP TABLE IF EXISTS `t_department`;
CREATE TABLE `t_department`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '部门id，主键自增',
  `d_name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_department
-- ----------------------------
INSERT INTO `t_department` VALUES (1, '开发部');
INSERT INTO `t_department` VALUES (2, '运营部');
INSERT INTO `t_department` VALUES (3, '市场部');
INSERT INTO `t_department` VALUES (4, '后勤部');

-- ----------------------------
-- Table structure for t_emp
-- ----------------------------
DROP TABLE IF EXISTS `t_emp`;
CREATE TABLE `t_emp`  (
  `id` int(6) NOT NULL AUTO_INCREMENT COMMENT 'id主键',
  `name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工姓名',
  `photopath` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工头像',
  `salary` double(20, 0) NULL DEFAULT NULL COMMENT '员工工资',
  `age` int(3) NULL DEFAULT NULL COMMENT '员工年龄',
  `d_id` int(10) NOT NULL COMMENT '员工所属部门id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_emp
-- ----------------------------
INSERT INTO `t_emp` VALUES (14, '不舒服的说法', 'd690b158-d72e-4a6a-af7f-c89276b4f357.ico', 45682, 18, 1);
INSERT INTO `t_emp` VALUES (15, '似懂非', '7cc9e545-6a75-4124-a04b-d7c2425cad7c.png', 45286, 18, 2);
INSERT INTO `t_emp` VALUES (16, '编程之外-plus', '603a634d-463a-4567-a481-43d8977d3536.jpg', 48695, 21, 1);
INSERT INTO `t_emp` VALUES (17, '路人甲', 'e5a9e38a-65cc-4276-a340-a91bcc91dd63.png', 45682, 18, 3);
INSERT INTO `t_emp` VALUES (20, '海绵宝宝', '7225efac-66cd-4051-9062-c9053f748fd8.jpg', 4568, 21, 3);
INSERT INTO `t_emp` VALUES (21, '阿狸', 'b6e9fe29-6a54-4b6a-a244-8f627aafef1b.jpg', 45681, 24, 1);
INSERT INTO `t_emp` VALUES (22, '潇洒哥', '77a31fb4-dfda-4763-a85b-5ff07b2fdb0c.jpg', 4568, 24, 4);
INSERT INTO `t_emp` VALUES (23, '懒洋洋', 'e5b8264d-40e7-45cc-9ae8-7c728d5f78bb.jpg', 25674, 16, 3);
INSERT INTO `t_emp` VALUES (24, '千寻', '155d24a4-f64a-49a7-bc4b-f039a32f6985.jpg', 45689, 22, 2);
INSERT INTO `t_emp` VALUES (25, '宫本武藏', '9b56a63c-68c6-4129-a730-f80708b0135d.jpg', 45612, 60, 4);
INSERT INTO `t_emp` VALUES (26, 'lingStudy', '7b10ff10-3811-4532-b065-3cf41f930d3e.jpg', 45687, 23, 2);
INSERT INTO `t_emp` VALUES (27, '灰太狼', 'e24f0d7b-fd2a-4ff1-b0c7-58adf9fc5ec4.jpg', 45621, 20, 3);
INSERT INTO `t_emp` VALUES (28, '千道流', '04ebf182-1d2b-4185-94d3-5d8653bf0619.jpg', 4568, 56, 1);
INSERT INTO `t_emp` VALUES (29, '没想好', '378f35f0-6b12-401d-bd13-2798fc9cc4a1.jpg', 45681, 56, 2);
INSERT INTO `t_emp` VALUES (30, '见鬼去吧', 'e1202c40-88d7-4130-9985-5d82a0786843.gif', 48562, 18, 2);
INSERT INTO `t_emp` VALUES (31, '你才见鬼', '86337cf7-2bd7-43d1-9ca4-0d9a97728713.gif', 45216, 17, 1);
INSERT INTO `t_emp` VALUES (33, '缓存后测试', 'f6a77b91-4e32-421f-a944-9ad7a33a06ac.gif', 48512, 20, 3);
INSERT INTO `t_emp` VALUES (35, '测试添加了部门', '4aa94f34-2287-4eee-ad60-3d013d2772c0.jpg', 45687, 18, 2);
INSERT INTO `t_emp` VALUES (37, '编写测试的', '7385b680-c458-4d48-8d7e-1d6ea22fdda5.jpg', 45687, 18, 2);

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
  `id` int(6) NOT NULL AUTO_INCREMENT COMMENT 'id主键',
  `username` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `realname` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '真实名',
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户密码',
  `gender` tinyint(1) NULL DEFAULT NULL COMMENT '用户性别 （1 男 0 女）',
  `registertime` datetime(0) NULL DEFAULT NULL COMMENT '用户注册时间',
  `state` tinyint(1) NULL DEFAULT NULL COMMENT '用户状态（是否启用，1 启用 0 未启用）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES (1, 'admin', NULL, '123456', NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (2, '编程之外', NULL, '123456', 1, '2020-07-09 17:56:16', 0);
INSERT INTO `t_user` VALUES (3, 'ling', NULL, '123456', 1, '2020-07-09 18:04:07', 0);
INSERT INTO `t_user` VALUES (4, 'xiang', NULL, '123456', 0, '2020-07-09 18:48:34', 0);
INSERT INTO `t_user` VALUES (5, '中文', NULL, '123456', 1, '2020-07-09 18:50:22', 0);
INSERT INTO `t_user` VALUES (6, 'lingStudy', NULL, '123456', 0, '2020-07-10 10:38:17', 0);
INSERT INTO `t_user` VALUES (7, '侧室', NULL, '123456', 1, '2020-07-11 19:53:09', 0);
INSERT INTO `t_user` VALUES (8, '0000', NULL, '123456', 0, '2020-07-11 21:31:02', 0);
INSERT INTO `t_user` VALUES (9, '1234', NULL, '123456', 0, '2020-07-11 21:34:16', 0);
INSERT INTO `t_user` VALUES (10, '测试000', NULL, '123456', 0, '2020-07-11 22:42:38', 0);

SET FOREIGN_KEY_CHECKS = 1;
