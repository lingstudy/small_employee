package com.ling.service;

import com.ling.pojo.Emp;

import java.util.List;

/**
 * @author 编程之外
 * @company lingStudy
 * @create 2020-07-10 23:46
 */
public interface EmpService {

    // 查询所有员工信息：注意 service层的参数是 当前页page，在实现类中再计算出 起始页pageStart 传给dao层
    // page:当前页码,起始页码 = (当前页码 -1)*每页条数
    public List<Emp> findAllEmp(String name, Integer page, Integer pageSize);

    // 查询数据总条数
    public int findEmpCounts(String name);

    //删除员工
    public int deleteEmpInfo(Integer id);

    //根据id查询要修改的员工信息
    public Emp findEmpById(Integer id);
    //修改员工信息
    public int updataEmp(Emp emp);

    //添加员工信息
    void save(Emp emp);


}
