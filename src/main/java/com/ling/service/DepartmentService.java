package com.ling.service;

import com.ling.pojo.Department;

import java.util.List;

/**
 * @author 编程之外
 * @company lingStudy
 * @create 2020-07-15 11:25
 */
public interface DepartmentService {
    //查询所有部门信息
    public List<Department> findAllDemt();

}
