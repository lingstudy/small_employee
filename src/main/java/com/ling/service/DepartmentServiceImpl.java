package com.ling.service;

import com.ling.dao.DepartmentDao;
import com.ling.pojo.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 编程之外
 * @company lingStudy
 * @create 2020-07-15 11:26
 */
@Service
@Transactional
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    private DepartmentDao departmentDao;

    @Override
    public List<Department> findAllDemt() {
        List<Department> allDemt = departmentDao.findAllDemt();
        return allDemt;
    }
}
