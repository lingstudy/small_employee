package com.ling.service;

import com.ling.dao.EmpDao;
import com.ling.pojo.Emp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author 编程之外
 * @company lingStudy
 * @create 2020-07-10 23:47
 */
@Service
@Transactional
public class EmpServiceImpl implements EmpService {

    @Autowired
    private EmpDao empDao;

    // 查询所有员工信息
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<Emp> findAllEmp(String name, Integer page, Integer pageSize) {
        //起始页码 = (当前页码 -1)*每页条数
        int pageStart = (page - 1) * pageSize;
        //调用dao层，根据name模糊查询，并分页
        List<Emp> empList = empDao.findAllEmp(name, pageStart, pageSize);
        return empList;
    }

    // 查询员工总数(数据条数)
    @Override
    public int findEmpCounts(String name) {
        int empCounts = empDao.findEmpCounts("%" + name + "%");
        return empCounts;
    }

    //删除员工
    @Override
    public int deleteEmpInfo(Integer id) {
        int empInfo = empDao.deleteEmpInfo(id);
        return empInfo;
    }

    //根据id查询要修改的员工信息
    @Override
    public Emp findEmpById(Integer id) {
        Emp empById = empDao.findEmpById(id);
        return empById;
    }

    //修改员工信息
    @Override
    public int updataEmp(Emp emp) {
        int updataEmp = empDao.updataEmp(emp);
        return updataEmp;
    }

    //添加员工信息
    @Override
    public void save(Emp emp) {
        empDao.addEmp(emp);
    }
}
