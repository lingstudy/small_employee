package com.ling.service;

import com.ling.pojo.User;

/**
 * @author 编程之外
 * @company lingStudy
 * @create 2020-07-09 15:32
 */
public interface UserService {

    //用户注册
    public void saveUser(User user);

    //用户登录
    public User login(User user);

}
