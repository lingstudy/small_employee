package com.ling.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 编程之外
 * @company lingStudy
 * @create 2020-09-11 21:51
 */
@RestController
public class TestController {

    @RequestMapping("/test")
    public String test(){
        return "项目启动成功！";
    }
}
