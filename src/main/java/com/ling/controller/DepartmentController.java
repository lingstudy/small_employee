package com.ling.controller;

import com.ling.pojo.Department;
import com.ling.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 编程之外
 * @company lingStudy
 * @create 2020-07-15 11:57
 */
@RestController
@CrossOrigin
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @GetMapping("/findDepList")
    public List<Department> findDepList(){
        List<Department> allDemt = departmentService.findAllDemt();
        return allDemt;
    }

}
