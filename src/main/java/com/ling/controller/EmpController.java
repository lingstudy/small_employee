package com.ling.controller;

import com.ling.pojo.Emp;
import com.ling.service.EmpService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author 编程之外
 * @company lingStudy
 * @create 2020-07-10 23:53
 */
@RestController
@CrossOrigin  //允许跨域请求
public class EmpController {

    @Autowired
    private EmpService empService;

    /**
     * 获取所有员工信息
     * 接收三个参数，也可以将这三个参数封装成一个对象，前端传递对应的对象即可
     */
    @GetMapping("/findAllEmp")
    public Map<String, Object> findAll(String name, Integer page, Integer pageSize) {
        System.out.println("前端传递来的三个参数分别为：" + name + "\t" + page + "\t" + pageSize);

        //page = page == null ? 1 : page;
        //pageSize = pageSize == null ? 5 : pageSize;

        // 调用service层获取 数据总条数
        int empCounts = empService.findEmpCounts(name);
        // 计算总页数，这里的分页用不到  总页数=总条数/每页显示的记录数
        //int sumPage = empCounts % pageSize == 0 ? empCounts / pageSize : empCounts / pageSize + 1;

        //调用service层获取 like 和 limit 查询后的所有员工信息
        List<Emp> empList = empService.findAllEmp(name, page, pageSize);

        Map<String, Object> map = new HashMap<>();
        map.put("empCounts", empCounts);
        map.put("empList", empList);
        return map;
    }

    //上传文件保存路径
    @Value("${upload.dir}")
    private String realPath;

    //添加员工
    // MultipartFile 文件上传 ，photo 参数名必须和前端传递的保持一致
    @PostMapping("/addEmp")
    public Map<String, Object> addEmp(Emp emp,@RequestParam("photo") MultipartFile photo) throws IOException {
        System.out.println("前端传递的要添加员工信息：" + emp);
        System.out.println("前端传递的图片文件信息：" + photo.getOriginalFilename());

        Map<String, Object> map = new HashMap<>();

        //保存路径
        //springboot 默认情况下只能加载 resource文件夹下静态资源文件
        //String realPath = "D:\\lingSoftware\\Java\\IDEA2019\\WorkSpace\\Project\\small_project\\employee\\src\\main\\resources\\static\\photos";

        //修改文件名
        String newFileName= UUID.randomUUID().toString()+"."+ FilenameUtils.getExtension(photo.getOriginalFilename());
        //头像保存（上传）
        photo.transferTo(new File(realPath,newFileName));

        //设置头像地址（要保存在数据库的字段）
        emp.setPhotopath(newFileName);

        try {
            //调用service层保存员工信息
            empService.save(emp);
            map.put("state",true);
            map.put("msg","添加成功！");
        } catch (Exception e) {
            e.printStackTrace();
            map.put("state",false);
            map.put("msg","添加失败！");
        }

        return map;
    }

    //删除员工信息
    @RequestMapping("/deleteEmp")
    public Map<String, Object> delete(Integer id) {
        Map<String, Object> map = new HashMap<>();

        /**
         * 调用Service层我们删除的只是数据库中的字段信息
         * 还应该删除对应上传的头像图片文件，
         * 删除员工信息前 先根据id查出员工对应的头像名称进而删除对应的头像
         * 然后，再调用service层删除数据库中对应的员工信息
         */

        //查询将要删除的员工(这里复用修改员工信息时的查询方法)
        Emp empById = empService.findEmpById(id);
        //拿到将要删除员工的头像( 即 realPath 路径下的 empById.getPhotopath() 文件)
        File file = new File(realPath, empById.getPhotopath());
        if (file.exists()){
            //1、删除头像文件
            file.delete();
        }
        //2、删除数据库中的员工信息
        int info = empService.deleteEmpInfo(id);
        if (info > 0) {
            map.put("state", true);
            map.put("msg", "删除成功！");
        } else {
            map.put("state", false);
            map.put("msg", "删除失败！");
        }
        return map;
    }

    //根据id获取要修改的员工信息
    @RequestMapping("/findUpdate")
    public Emp findByIdEmp(Integer id) {
        Emp empById = empService.findEmpById(id);
        return empById;
    }

    //修改员工信息
    @PostMapping("/update")
    public int updateEmp(@RequestBody Emp emp) {
        System.out.println("前端传来的员工信息：" + emp);
        int i = empService.updataEmp(emp);
        return i;
    }

}









