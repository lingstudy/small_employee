package com.ling.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author 编程之外
 * 实现 ApplicationContextAware 拿到spring自动创建的工厂
 */
@Component
public class ApplicationContextUtils implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext=applicationContext;
    }

    // 根据name获取对应的对象  redisTemplate  StringRedisTemplate
    public static Object getBean(String name){
        return applicationContext.getBean(name);
    }

}
