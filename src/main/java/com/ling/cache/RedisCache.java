package com.ling.cache;

import com.ling.utils.ApplicationContextUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.cache.Cache;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @author 编程之外
 * 自定义缓存配置类
 * Cache是由mybatis初始化的，所以这里spring工厂是拿不到对象的创建的
 * 需要编写一个工具类拿到spring自动创建的工厂  才能spring的注入语法
 */
@Slf4j
public class RedisCache implements Cache {

    //准备属性
    private String id;
    public RedisCache(String id) {
        //当前缓存的id:[com.ling.dao.EmpDao]
        log.info("当前缓存的id:[{}]",id);
        this.id = id;
    }

    @Override
    public String getId() {
        //namespace
        return this.id;
    }

    @Override  //放入redis缓存
    public void putObject(Object key, Object value) {
        System.out.println("放入缓存的key："+key);
        System.out.println("放入缓存的value："+value);
        getRedisTemplate().opsForHash().put(id,key.toString(),value);
    }

    //自定义方法拿到 redisTemplate
    public RedisTemplate getRedisTemplate(){
        //工具name拿到redisTemplate对象
        RedisTemplate redisTemplate = (RedisTemplate) ApplicationContextUtils.getBean("redisTemplate");
        // 将RedisTemplate对象中的序列化方式转化成String方式
        //字符串deKey
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        //Hash的Key
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        return redisTemplate;
    }

    @Override  //从redis缓存中获取
    public Object getObject(Object key) {
        log.info("获取缓存的key:[{}]",key.toString());
        return getRedisTemplate().opsForHash().get(id,key.toString());
    }

    @Override //删除指定的缓存信息
    public Object removeObject(Object o) {
        return null;
    }

    @Override  //清除缓存信息
    public void clear() {
        log.info("清除所有缓存信息...");
        getRedisTemplate().delete(id);
    }

    @Override  // 数量
    public int getSize() {
        return getRedisTemplate().opsForHash().size(id).intValue();
    }
}
