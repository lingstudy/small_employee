package com.ling.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author 编程之外
 * @company lingStudy
 * @create 2020-07-10 23:13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
//设置支持链式调用
@Accessors(chain = true)
public class Emp implements Serializable {
    private Integer id;
    private String name;
    private String photopath;
    private Double salary;
    private Integer age;
    private Integer d_id;

    private Department department;
}
