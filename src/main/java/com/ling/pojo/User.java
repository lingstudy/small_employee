package com.ling.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author 编程之外
 * @company lingStudy
 * @create 2020-07-09 12:59
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class User {
    private String id;//String 类型的api相当多，方便处理
    private String username;
    private String realname;
    private String password;
    private boolean gender; //用户性别 （1 男 0 女）
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date registertime;
    //数据库一个表中有一个tinyint类型的字段，值为0或者1，如果取出来的话，0会变成false，1会变成true。
    private boolean state;  //用户状态（是否启用，1 启用 0 未启用）
}
