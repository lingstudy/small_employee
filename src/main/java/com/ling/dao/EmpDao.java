package com.ling.dao;

import com.ling.pojo.Emp;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 编程之外
 * @company lingStudy
 * @create 2020-07-10 23:21
 */
@Mapper
@Repository
public interface EmpDao {

    // 查询所有员工信息  动态SQL处理 name参数：搜索时，根据name模糊查询；
    public List<Emp> findAllEmp(@Param("name") String name,@Param("pageStart") Integer pageStart,@Param("pageSize") Integer pageSize);
    // 查询员工总数(数据条数)
    public int findEmpCounts(String name);

    //删除员工
    public int deleteEmpInfo(Integer id);

    //根据id查询要修改的员工信息
    public Emp findEmpById(Integer id);
    //修改员工信息
    public int updataEmp(Emp emp);

    //添加员工信息
    void addEmp(Emp emp);
}
