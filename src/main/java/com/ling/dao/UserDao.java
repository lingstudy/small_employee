package com.ling.dao;

import com.ling.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @author 编程之外
 * @company lingStudy
 * @create 2020-07-09 15:26
 */
@Mapper
@Repository
public interface UserDao {

    // 注册前先判断该用户名是否已存在
    // (该方法不需要再新建一个service接口，直接在UserServiceImpl的注册方法中调用该方法即可)
    // 用户登录可复用该方法
    public User findByUserName(String username);
    //用户注册
    public void saveUser(User user);

}
