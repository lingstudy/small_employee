package com.ling.dao;

import com.ling.pojo.Department;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 编程之外
 * @company lingStudy
 * @create 2020-07-15 9:42
 */
@Mapper
@Repository
public interface DepartmentDao {

    //查询所有部门信息
    public List<Department> findAllDemt();
}
