package com.ling;

import com.ling.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.Date;

@SpringBootTest
class EmployeeApplicationTests {

    @Test
    void contextLoads() {
        //User user = new User();
        //user.setGender(true);
        //// getGender() => isGender()
        //System.out.println(user.isGender());
        //System.out.println("-===========-");
        ////Thu Jul 09 15:38:05 CST 2020
        //System.out.println(new Date());
        //User registertime = user.setRegistertime(new Date());
        //System.out.println(registertime);
    }

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    //操作对象
    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void testRedis(){
        stringRedisTemplate.opsForValue().set("name","编程之外");
        System.out.println(stringRedisTemplate.opsForValue().get("name"));
    }

}
